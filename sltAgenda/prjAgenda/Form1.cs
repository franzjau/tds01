﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace prjAgenda
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            Lista();
        }

        private void Lista()
        {
            MySqlConnection conn;
            string conString;

            conString = "server=localhost; user id=root;password=root;database=agenda;";
            conn = new MySqlConnection(conString);
            conn.Open();

            MySqlDataAdapter adapter = new MySqlDataAdapter("select * from tblagenda", conn);
            DataSet ds = new DataSet();

            adapter.Fill(ds);

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = ds.Tables[0];

            conn.Close();
        }
        private void btnIncluir_Click(object sender, EventArgs e)
        {

            try
            {
                MySqlConnection conn;
                MySqlCommand _Comando;
                string conString;

                conString = "server=localhost; user id=root;password=root;database=agenda;";
                conn = new MySqlConnection(conString);

                conn.Open();

                _Comando = new MySqlCommand("Insert Into tblAgenda (Nome, Endereco, Cidade, Estado, Telefone, Whats, Facebook, Linkedin, Twitter, Instagram) values (@Nome, @Endereco, @Cidade, @Estado, @Telefone, @Whats, @Facebook, @Linkedin, @Twitter, @Instagram)", conn);
                _Comando.Parameters.AddWithValue("@Nome", txtNome.Text);
                _Comando.Parameters.AddWithValue("@Endereco", txtEndereco.Text);
                _Comando.Parameters.AddWithValue("@Cidade", txtCidade.Text);
                _Comando.Parameters.AddWithValue("@Estado", cmbEstado.Text);
                _Comando.Parameters.AddWithValue("@Telefone", txtTelefone.Text);
                _Comando.Parameters.AddWithValue("@Whats", txtWhats.Text);
                _Comando.Parameters.AddWithValue("@Facebook", txtFacebook.Text);
                _Comando.Parameters.AddWithValue("@Linkedin", txtLinkedin.Text);
                _Comando.Parameters.AddWithValue("@Twitter", txtTwitter.Text);
                _Comando.Parameters.AddWithValue("@Instagram", txtInstagram.Text);
                _Comando.ExecuteNonQuery();

                conn.Close();

                Lista();

                MessageBox.Show("Informação Gravada com Sucesso");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {

            try
            {
                if (txtID.Text.Length <= 0)
                {
                    MessageBox.Show("Nro do ID Inválido");
                    return;
                }
                MySqlConnection conn;
                string conString;

                conString = "server=localhost; user id=root;password=root;database=agenda;";
                conn = new MySqlConnection(conString);
                conn.Open();

                MySqlDataAdapter adapter = new MySqlDataAdapter("select * from tblagenda where id = " + txtID.Text, conn);
                DataSet ds = new DataSet();

                adapter.Fill(ds);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtNome.Text = ds.Tables[0].Rows[0]["Nome"].ToString();
                    txtEndereco.Text = ds.Tables[0].Rows[0]["Endereco"].ToString();

                }
                else
                {
                    txtNome.Text = "";
                    txtEndereco.Text = "";
                    MessageBox.Show("Não foi encontrado nenhum registro");
                }
                conn.Close();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return;
            }
        }
    }
}
