var OpenWeatherAppKey = "132bba4f20384f1c1e1443faa0e5cb73";

function getWeatherWithZipCode() {
    var zipcode = $('#zip-code-input').val();
    var nome = $('#nome-input').val();
    var email = $('#email-input').val();
    var telefone = $('#telefone-input').val();


    console.log(zipcountry);

    //var queryString = 'http://api.openweathermap.org/data/2.5/weather?zip=' + zipcode + '&appid=' + OpenWeatherAppKey + '&units=imperial';

    var queryString = "http://localhost/cadastro.php?M1="+nome+"&M2="+email+"&M3="+telefone+"";
    console.log(queryString);

    $.getJSON(queryString, function(results) {
        showWeatherData(results);
        console.log(results);
    }).fail(function(jqXHR) {
        $('#error-msg').show();
        $('#error-msg').text("Error retrieving data. " + jqXHR.statusText);
        console.log(jqXHR.statusText);
    });
    return false;
}

function showWeatherData(results) {

    if (results.weather.length) {
        $('#error-msg').hide();
        $('#weather-data').show();

        $('#title').text(results.name);
        $('#temperature').text(results.main.temp);
        $('#wind').text(results.wind.speed);
        $('#humidity').text(results.main.humidity);
        $('#visibility').text(results.weather[0].main);

        var sunriseDate = new Date(results.sys.sunrise * 1000);
        $('#sunrise').text(sunriseDate.toLocaleTimeString());

        var sunsetDate = new Date(results.sys.sunset * 1000);
        $('#sunset').text(sunsetDate.toLocaleTimeString());

    } else {
        $('#weather-data').hide();
        $('#error-msg').show();
        $('#error-msg').text("Error retrieving data. ");
    }
}